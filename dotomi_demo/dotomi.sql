-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : mar. 29 nov. 2022 à 15:10
-- Version du serveur :  10.4.11-MariaDB
-- Version de PHP : 7.4.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `dotomi`
--

-- --------------------------------------------------------

--
-- Structure de la table `blogs`
--

CREATE TABLE `blogs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `user_id` int(11) NOT NULL,
  `title` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `image` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `is_lock` tinyint(1) NOT NULL DEFAULT 0,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `blogs`
--

INSERT INTO `blogs` (`id`, `user_id`, `title`, `image`, `content`, `is_lock`, `created_at`, `updated_at`) VALUES
(1, 3, 'Plaisir et Epanouissement sexuel', 'IMG20221129-115718.jpg', '<p>Une bonne santé sexuelle et reproductive est un état de bien-être total sur le plan physique, mental et social, relativement à tous les aspects du système reproductif. Dans cet état, les personnes sont en mesure de profiter d\'une&nbsp; vie sexuelle satisfaisante et sûre. Il est nécessaire que toutes les personnes doivent être informées et habilitées à se protéger des *infections sexuellement transmissible, de l\'hygiène menstruel et corporel et du plaisir épanouissement sexuel.&nbsp;Parlant du plaisir épanouissement sexuel il faut retenir que tout les plaisir ne donne toujours pas un épanouissement positif comme (sentiment agréable, sensation sexuelle, la tendresse, confiance mutuelle ...) , il peut arriver que le plaisir donne un épanouissement négatif comme (la peur, la culpabilité, le goût etc..).<br></p>', 0, '2022-11-29 10:57:18', '2022-11-29 10:57:18'),
(2, 3, 'Infections Sexuellement Transmissibles', 'IMG20221129-115846.jpg', '<p>Une bonne santé sexuelle et reproductive est un état de bien-être total sur le plan physique, mental et social, relativement à tous les aspects du système reproductif. Dans cet état, les personnes sont en mesure de profiter d\'une&nbsp; vie sexuelle satisfaisante et sûre. Il est nécessaire que toutes les personnes doivent être informées et habilitées à se protéger des infections sexuellement transmissible, de l\'hygiène menstruel et corporel et du plaisir épanouissement sexuel.</p><p>Parlant des infections sexuellement transmissible,elles sont transmises par les rapports sexuels non protégés,par la transfusion sanguine et de la mère à l\'enfant pendant la grossesse ou au cours de l\'accouchement. Les infections les plus courantes sont:</p><p>La gonococcie ou chaude pisse,la syphilis, les chlamydiases,les trichomonases,d\'écoulement chez la femme et chez l\'homme etc...Elles se manifestent par des brûlures en urinant, le gonflement sous la peau, la plaie sur l\'appareil génital ou le bouton,des perte blanches avec ou sans odeurs etc...Elles ont des conséquences comme l\'infertilité, la stérilité, le décès, l\'amputation de la verge, le cancer du col, la trompe bouchée etc..</p><p><br></p>', 0, '2022-11-29 10:58:46', '2022-11-29 10:58:46'),
(3, 3, 'Hygiene menstruel et Corporel', 'IMG20221129-120039.jpg', '<p>Une bonne santé sexuelle et reproductive est un état de bien-être total sur le plan physique, mental et social, relativement à tous les aspects du système reproductif. Dans cet état, les personnes sont en mesure de profiter d\'une&nbsp; vie sexuelle satisfaisante et sûre. Il est nécessaire que toutes les personnes doivent être informées et habilitées à se protéger des *infections sexuellement transmissible, de l\'hygiène menstruel et corporel et du plaisir épanouissement sexuel.&nbsp;<span style=\"text-align: var(--bs-body-text-align);\">Quand on parle de l\'hygiène menstruel et corporel c\'est un phénomène normal, mais les changements qui se produisent dans l\'organisme exigent une observation rigoureuse des règles d\'hygiène.</span><br></p><p>Le sang menstruel doit s\'écouler librement et être absorbé dans des garnitures propres, les organes génitaux externes seront lavés deux ou trois fois par jours à l\'eau propre, il est conseillé d\'éviter les rapports sexuels pendant les règles, pendant les menstrues, il est recommandé de prendre des douches et non des bains(marigots,piscine, baignoires) car les microbes éventuels contenus dans l\'eau salie du bain pourraient pénétrer dans le vagin et dans la cavité utérine provoquant ainsi une infection de la muqueuse utérine.</p>', 0, '2022-11-29 11:00:39', '2022-11-29 11:00:39');

-- --------------------------------------------------------

--
-- Structure de la table `contacts`
--

CREATE TABLE `contacts` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `subject` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `phone` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `contacts`
--

INSERT INTO `contacts` (`id`, `name`, `email`, `subject`, `phone`, `content`, `created_at`, `updated_at`) VALUES
(1, 'dgirls', 'dgirls@gmail.com', 'messagerie', '45678900', 'superbe équipe', '2022-11-29 12:40:37', '2022-11-29 12:40:37');

-- --------------------------------------------------------

--
-- Structure de la table `failed_jobs`
--

CREATE TABLE `failed_jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `uuid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `connection` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `queue` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `exception` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `failed_at` timestamp NOT NULL DEFAULT current_timestamp()
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `messages`
--

CREATE TABLE `messages` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `from_id` int(11) NOT NULL,
  `to_id` int(11) NOT NULL,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `read_at` int(11) DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `messages`
--

INSERT INTO `messages` (`id`, `from_id`, `to_id`, `content`, `read_at`, `created_at`, `updated_at`) VALUES
(1, 3, 2, 'salut madame', 1, '2022-11-29 11:02:39', '2022-11-29 11:03:31'),
(2, 2, 3, 'comment allez vous?', NULL, '2022-11-29 11:03:41', '2022-11-29 11:03:41'),
(3, 4, 2, 'salut madame je suis nouveau', 1, '2022-11-29 11:11:24', '2022-11-29 11:13:09'),
(4, 2, 4, 'comment allez vous monsieur? Bienvenue sur Dotomi', 1, '2022-11-29 11:13:28', '2022-11-29 11:13:41'),
(5, 4, 2, 'merci', 1, '2022-11-29 11:13:50', '2022-11-29 11:14:25'),
(6, 11, 2, 'salut docteur', 1, '2022-11-29 12:33:47', '2022-11-29 12:36:24'),
(7, 2, 11, 'comment allez vous?', 1, '2022-11-29 12:36:34', '2022-11-29 12:36:49'),
(8, 11, 2, 'bien merci', NULL, '2022-11-29 12:36:57', '2022-11-29 12:36:57');

-- --------------------------------------------------------

--
-- Structure de la table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2019_08_19_000000_create_failed_jobs_table', 1),
(4, '2019_12_14_000001_create_personal_access_tokens_table', 1),
(5, '2022_11_11_112848_create_blogs_table', 1),
(6, '2022_11_12_025944_create_messages_table', 1),
(7, '2022_11_19_140332_create_contacts_table', 1),
(8, '2022_11_19_140347_create_newsletters_table', 1),
(9, '2022_11_20_061859_create_ratings_table', 1);

-- --------------------------------------------------------

--
-- Structure de la table `newsletters`
--

CREATE TABLE `newsletters` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `email` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `personal_access_tokens`
--

CREATE TABLE `personal_access_tokens` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `tokenable_type` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `tokenable_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(64) COLLATE utf8mb4_unicode_ci NOT NULL,
  `abilities` text COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `last_used_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Structure de la table `ratings`
--

CREATE TABLE `ratings` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `star` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `visible` tinyint(1) NOT NULL DEFAULT 0,
  `content` text COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `ratings`
--

INSERT INTO `ratings` (`id`, `name`, `star`, `visible`, `content`, `created_at`, `updated_at`) VALUES
(1, 'essaie', '3', 0, 'je suis heureux', '2022-11-29 11:15:21', '2022-11-29 11:15:21');

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `pseudo` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `phone` int(11) DEFAULT NULL,
  `city` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `work` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `birth` timestamp NULL DEFAULT NULL,
  `sex` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `role` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'user',
  `image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `online` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '0',
  `is_active` tinyint(1) NOT NULL DEFAULT 1,
  `email_verified_at` timestamp NULL DEFAULT NULL,
  `password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `pseudo`, `email`, `phone`, `city`, `work`, `birth`, `sex`, `role`, `image`, `online`, `is_active`, `email_verified_at`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'Ange DEGO', 'Ange96', 'ange96@gmail.com', 98741253, 'Cotonou', 'Génycoloque Andrologue', NULL, NULL, 'admin', NULL, '0', 1, NULL, '$2y$10$jzV14jsKUfXfiZQHJvwpXel5XOHwQvVzkUYjXofEAhsXjXSaqYwb6', NULL, '2022-11-29 10:48:24', '2022-11-29 12:40:54'),
(2, 'Ruth BANGA', 'Ruth01', 'ruth01@gmail.com', 99874123, 'Cotonou', 'Andrologue', NULL, NULL, 'gyneco', NULL, '0', 1, NULL, '$2y$10$rDnTzIo9l587Ae0cYnecCePbe0XDtITfO66IZi0V0t6hy3Rv2IJwW', NULL, '2022-11-29 10:48:24', '2022-11-29 12:37:30'),
(3, 'Pierre NAGO', 'Pierre03', 'pierre03@gmail.com', 62145879, 'Cotonou', 'Pair Educateur', NULL, NULL, 'teach', NULL, '0', 1, NULL, '$2y$10$DM0exBTGjOhtp/P6d0oHYeXB2CIR3YSlKGs5CzmvusydeEVeubd8O', NULL, '2022-11-29 10:48:24', '2022-11-29 12:38:51'),
(4, NULL, 'essaie', NULL, 66018409, 'calavi', 'menusier', '2022-11-09 23:00:00', 'Masculin', 'user', 'IMG20221129-1214084.jfif', '0', 1, NULL, '$2y$10$pINfmyt8NkUThe7kDndg8uYXkSlKHHrNrGe6tbL5HqnSjT/jnEXfu', NULL, '2022-11-29 11:10:10', '2022-11-29 11:15:27'),
(11, NULL, 'dgirls', NULL, 61569869, 'Cotonou', 'institeur', '2022-11-02 23:00:00', 'Féminin', 'user', 'IMG20221129-13345111.jfif', '1', 1, NULL, '$2y$10$N2ZDPZq8g8ou3iu0qebk5eX3elix8unkEy44JB.H2LCyDcHKtvPPe', NULL, '2022-11-29 12:33:02', '2022-11-29 12:35:52');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `blogs`
--
ALTER TABLE `blogs`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `contacts`
--
ALTER TABLE `contacts`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `failed_jobs_uuid_unique` (`uuid`);

--
-- Index pour la table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `newsletters`
--
ALTER TABLE `newsletters`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Index pour la table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `personal_access_tokens_token_unique` (`token`),
  ADD KEY `personal_access_tokens_tokenable_type_tokenable_id_index` (`tokenable_type`,`tokenable_id`);

--
-- Index pour la table `ratings`
--
ALTER TABLE `ratings`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_pseudo_unique` (`pseudo`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `blogs`
--
ALTER TABLE `blogs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `contacts`
--
ALTER TABLE `contacts`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `failed_jobs`
--
ALTER TABLE `failed_jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT pour la table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT pour la table `newsletters`
--
ALTER TABLE `newsletters`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `personal_access_tokens`
--
ALTER TABLE `personal_access_tokens`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT pour la table `ratings`
--
ALTER TABLE `ratings`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
